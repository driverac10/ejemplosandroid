
import java.io.*;
import java.net.*;

public class SaveHolaTxt {
    public static void main(String[ ] args) throws IOException {

        Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        // Datos de la conexion
        final String host = args[0];
        final int port = 80;

        try {
            // Establece conexion (TCP)
            socket = new Socket(host, port);

            // Obtiene canales de entrada y salida
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                                        socket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + host);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for " + host);
            System.exit(1);
        }

        // Escribe la peticion:
        final String url = args[1];
		System.out.println("GET " + url + " HTTP/1.0");
        out.println("GET " + url + " HTTP/1.0");
		System.out.println("Host:" + host);
        out.println("Host:" + host);
		System.out.println();
        out.println();

        // Se asegura que se transmite la peticion completa
        out.flush();

        // Se lee la cabecera buscando la longitud del contenido
        int longitud = -1;

        System.out.println("---- Cabecera ----");
        String aux = in.readLine();
        while ( aux.length() > 0 ) { // Acaba con una linea en blanco
            System.out.println("#" + aux + "#");
            if ( aux.contains("Content-Length") ) {
                aux = aux.replace(':', '\n');
                BufferedReader sr = new BufferedReader(
                                            new StringReader(aux));
                sr.readLine();
                longitud = Integer.parseInt( sr.readLine().trim() );
            }
            aux = in.readLine();
        }

        // Graba el contenido en un archivo
		if ( longitud > 0 ) {
			int i;
			FileWriter texto = new FileWriter("result.txt");
			for ( i = 0; i < longitud; ++i) {
				int dato = in.read();
				texto.write( dato );
				//System.out.print( (char)dato );
			}
			texto.close();
		}
		else {
			PrintWriter texto = new PrintWriter(new FileWriter("result.txt"));
			try {
				String linea = in.readLine();
				while ( linea != null ) {
					texto.println("#" + linea + "#");
					linea = in.readLine();
				}
			}
			catch ( IOException e ) {
				texto.println(e.getMessage());
			}
			texto.close();
		}
        out.close();
        in.close();
        socket.close();
    }
}
