
import java.io.*;
import java.net.*;

public class DayTime {
    public static void main(String[ ] args) throws IOException {

        Socket socket = null;
        PrintWriter out = null;
        BufferedReader in = null;

        // Datos de la conexion
        final String host = args[0];
        final int port = 13;

        try {
            // Establece conexion (TCP)
            socket = new Socket(host, port);

            // Obtiene canales de entrada y salida
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(
                                        socket.getInputStream()));
        } catch (UnknownHostException e) {
            System.err.println("Don't know about host: " + host);
            System.exit(1);
        } catch (IOException e) {
            System.err.println("Couldn't get I/O for " + host);
            System.exit(1);
        }

        // Escribe la peticion:
        out.println("Hola, hola");
        out.flush(); //Se envia la peticion

        // Vamos a procesar la respuesta
        System.out.println("---- Respuesta ----");
		
		try {
			String aux = in.readLine();
			while ( aux != null ) {
				System.out.println("#" + aux + "#");
				aux = in.readLine();
			}
		}
		catch ( IOException e ) {
		}

        out.close();
        in.close();
        socket.close();
    }
}
