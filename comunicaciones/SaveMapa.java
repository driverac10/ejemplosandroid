
import java.io.*;
import java.net.*;
import java.nio.file.*;

public class SaveMapa {
    public static void main(String[ ] args) {

        // URL del recurso, en realidad lo que conocemos como
        // "direccion de internet"
        final String picture_url = args[0];
        try {
            // Abre la conexion TCP y obtiene la respuesta HTTP
            // sin que el programador tenga que conocer los detalles.
            URL url = new URL(picture_url);
            URLConnection con = url.openConnection();
            con.connect();

            // La informacion recibida se encuentra en el Stream de
            // entrada, el resto es para copiar esa informacion a disco
            Path path = FileSystems.getDefault().getPath("staticmap.png");
            Files.copy(con.getInputStream(), path);

        } catch (IOException e) {
            System.out.println( e );
            System.out.println( "Could not get img from: " + picture_url);
        }
    }
}
