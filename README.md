# README #

Este repositorio contiene fundamentalmente material docente para los alumnos de la asignatura de Programación en Android de la ETSII-UPM. Por esto toda la documentación y programación aparecerá preferentemente en Español. 

No creo que los ejemplos que aquí aparecen puedan tener utilidad profesional directa, no obstante, si alguien tiene interés en usarlos pueden considerarse en el dominio público (public domain). Para uso educativo, los ejemplos debería considerarse dentro de una licencia de tipo Creative Commons, si alguien tiene interés en usarlos con fines educativos por favor contacte con el autor.

## Ejemplos de java en 3 segundos ##

Son ejemplos muy básicos que ilustran aspectos básicos del lenguaje: atributos, métodos, constructores, atributos de clase. Y otros no tan básicos, pero simplificados al máximo: polimorfismo o plantillas. 

Para compilar estos archivos se recomienda utilizar directamente el compilador de línea de comandos: javac. 

Simplemente cambiar a la carpeta y hacer:

    javac *.java

El único archivo que tiene un programa es Main.java (el resto son clases sueltas). Para ejecutarlo hacer: 

    java Main

Como actividad se recomienda modificar alguno de los ejemplos, especialmente los últimos. Ejemplos que puede realizar el alumno:

1. Hacer otro clase derivada de HacerOperacion, por ejemplo el producto.
1. Hacer otra operación sobre el vector, por ejemplo, calcular el máximo o el mínimo.
1. Emplea otra clase de plantilla de la familia de los contenedores. Prueba primero con una lista y luego con un *Dictionary*, es mucho más interesante, pero un poco más complejo, sin ir más lejos tiene 2 parámetros de plantilla. 
1. Piensa e implementa un algoritmo que opere sobre un vector de objetos y que además aplique polimorfismo. 


## Comunicaciones ##

Para usar:

    java SaveHolaTxt "maps.googleapis.com" "/maps/api/directions/json?origin=Toronto&destination=Montreal"

    "http://maps.googleapis.com/maps/api/staticmap?center=-15.800513,-47.91378&zoom=11&size=200x200&sensor=false"

    