
public class Polimorfismo {
    
    HacerOperacion op1;
    HacerOperacion op2;
    
    EjemploThis dato;
    
    public Polimorfismo() {
        dato = new EjemploThis();
        op1 = new HacerOperacion();
        op2 = new Suma();
    }
       
    public void algoritmo() {
        dato.hago(op1, 2);
        dato.hago(op2, 3);
    }
}
