
public class Suma extends HacerOperacion {
    
    @Override
    public int obtenerResultado(EjemploThis in, int cantidad) {
        return in.numero + cantidad;
    }
}
