
public class EjemploThis {
    
    public int numero;
    
    public EjemploThis() {
        numero = 10;
    }
       
    public void hago(HacerOperacion op, int cantidad) {
        numero = op.obtenerResultado(this, cantidad);
    }
}
