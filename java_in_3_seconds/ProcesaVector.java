
import java.util.Vector;

public interface ProcesaVector {
    
    double hazOperacion( Vector<Double> v );
    
}
