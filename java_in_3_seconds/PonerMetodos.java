
public class PonerMetodos {
    
    public int numero;
    
    public String mi_nombre;
    
    private String nombre_del_vecino;
    
    public PonerMetodos(String vecino) {
        mi_nombre = new String("Juan Español");
        nombre_del_vecino = vecino;
        numero = 10;
    }
    
    public void mostrar() {
        System.out.println(mi_nombre + numero);
    }
    
    public String crearMensaje(String saludo) {
        String msg = new String(saludo);
        msg.concat("a mi vecino : ");
        msg.concat(nombre_del_vecino);
        return msg;
    }
    
    public void doyNumero(EjemploAtributos atr, int cantidad) {
        numero -= cantidad;
        atr.numero += cantidad;
    }
    
}
