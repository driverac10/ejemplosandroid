
import java.util.Vector;
import java.util.Iterator;

public class Media implements ProcesaVector {
    
    public double hazOperacion( Vector<Double> v ) {
        double suma = 0;
        for ( double x : v ) {
            suma += x;
        }
        return suma / v.size();
    }
    
    public double alternativa( Vector<Double> v ) {
        Iterator<Double> ite = v.iterator();
        double suma = 0;
        while ( ite.hasNext() ) {
            suma += ite.next();
        }
        return suma / v.size();
    }
    
}
