
public class EjemploEstaticos {
    
    static HacerOperacion op1 = new HacerOperacion();
    HacerOperacion op2;
    
    static EjemploThis dato = new EjemploThis();
    
    public EjemploEstaticos() {
        op2 = new Suma();
    }
       
    static public void unaOperacion() {
        dato.hago(op1, 2);
        // Prohibido usar this y op2
    }

    public void dosOperaciones() {
        EjemploEstaticos.unaOperacion();
        dato.hago(op2, Math.abs(-3));
    }
}
